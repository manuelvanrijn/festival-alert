var App = function(host, port) {
  var _host = host;
  var _port = port;
  var _socket;

  var prepareSocketIO = function() {
    _socket = io.connect(_host, {
      port: _port
    });

    // Error connecting
    _socket.on('connect_failed', function(){
      noInternetOrGeolocation();
    });
    _socket.on('disconnect', function () {
      noInternetOrGeolocation();
    });

    // Ready to serve
    _socket.on('connect', function(){
      $(".loader").hide();
      $(".actions").show();
    });
  };

  var sendAlert = function(position, alertType) {
    _socket.emit('alert', {
      lat: position.coords.latitude,
      lng: position.coords.longitude,
      type: alertType
    });
  };

  /**
  * Check if there is a internet connection
  *
  * @return {Boolean} Returns true if the device has a connection
  */
  var hasConnection = function() {
    var networkState = navigator.connection.type;
    return (networkState.type === Connection.UNKNOWN || networkState.type === Connection.NONE) ? false : true;
  };

  /**
  * Event triggered when device is ready
  */
  var onDeviceReady = function() {
    // Check for internet connection
    if(hasConnection() == false) {
      noInternetOrGeolocation();
    }

    // Prefetch location
    navigator.geolocation.getCurrentPosition(function(position) {}, noInternetOrGeolocation);
  };

  var noInternetOrGeolocation = function() {
    $("section").hide();
    $("#offline-nogeo").show();
  };

  var onLocationError = function(error) {
    noInternetOrGeolocation();
  };

  var init = function() {
    $(function() {
      document.addEventListener('deviceready', onDeviceReady, false);
      prepareSocketIO();
      $(".alert").on("touchend", function() {
        $this = $(this);
        var alertType = eval($this.data('type'));
        var alertTitle = alertType.title;
        var icon = $(this).find("i").clone();

        $("#alertModal .bar-title h1").html(icon.html() + " " + alertTitle);
        navigator.geolocation.getCurrentPosition(function(position) {
          sendAlert(position, alertType);
          $(".pending").hide();
          $(".finished").show();
        }, onLocationError);
      });

      // reset modal on close
      $("#alertModal .bar-title .button").on("touchend", function() {
        $(".pending").delay(800).show(0, function() {
          $(".finished").hide(0);
          $("textarea").val("");
        });
      });
    });
  };

  init();

  return {
    sendAlert: sendAlert
  };
};
