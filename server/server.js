var express = require('express')
    , http = require('http')
    , path = require('path')
    , io = require('socket.io');

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);

app.use(allowCrossDomain);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);

// development only
if ('production' == app.get('env')) {
  app.use(express.static(path.join(__dirname, 'dist')));
} else {
  app.use(express.static(path.join(__dirname, 'app')));
  app.use(express.static(path.join(__dirname, '.tmp')));
  app.use(express.errorHandler());
}

var server = http.createServer(app).listen(app.get('port'), function () {
  console.log("Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});
io.listen(server).sockets.on('connection', function(socket) {
  socket.on('alert', function(data) {
    console.log("Recieved Alert: '" + data.type.title + "' at location, lat: " + data.lat + " lng: " + data.lng);
    socket.broadcast.emit('alert', data);
  })
});
