'use strict'

angular.module('festivalAlertServerApp')
  .factory 'SocketService', ['$rootScope', ($rootScope) ->
    socket = io.connect()

    subscribe = (channel, callback) ->
      socket.on channel, ->
        args = arguments
        $rootScope.$apply ->
          callback.apply socket, args

    submit = (channel, data, callback) ->
      socket.emit channel, data, ->
        args = arguments
        $rootScope.$apply ->
          callback? callback.apply socket, args

    {
      subscribe
      submit
    }
  ]
