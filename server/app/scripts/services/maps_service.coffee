'use strict'

angular.module('festivalAlertServerApp')
  .factory 'MapsService', ->
    map = null
    bounds =
      southWest: new google.maps.LatLng 52.048910, 4.273982
      northEast: new google.maps.LatLng 52.062729, 4.296026

    randomLatLng = ->
      lngSpan = bounds.northEast.lng() - bounds.southWest.lng();
      latSpan = bounds.northEast.lat() - bounds.southWest.lat();
      new google.maps.LatLng(
        bounds.southWest.lat() + latSpan * Math.random(),
        bounds.southWest.lng() + lngSpan * Math.random());

    addWorkerMarker = (lat=0, lng=0) ->
      latlng = randomLatLng()
      image = "/images/markers/worker.png"
      addMarker latlng, image, false

    addAlertMarker = (lat, lng, alert_type) ->
      if lat? and lng?
         latlng = new google.maps.LatLng lat, lng
      else
        latlng = randomLatLng()
      image = alert_type.image
      addMarker latlng, image

    addMarker = (latlng, image, centerMap=true) ->
      map = getMap()
      map.panTo latlng
      map.setZoom 17
      marker = new google.maps.Marker
        position: latlng
        map: getMap()
        # animation: google.maps.Animation.BOUNCE
        animation: google.maps.Animation.DROP
        icon: image

      # stop bounce after 3 times
      # setTimeout ->
      #   marker.setAnimation null
      # , 2200

    getMap = ->
      map ?= initMap()

    initMap = ->
      zuiderpark = new google.maps.LatLng 52.055093, 4.286303
      mapGetTile = (x,y,z) ->
        "/images/tiles/" + z + "/" + x + "/" + y + ".png";

      map = new google.maps.Map document.getElementById('map-canvas'),
        streetViewControl: false
        zoomControl: true
        zoom: 16
        center: zuiderpark
        mapTypeId: google.maps.MapTypeId.HYBRID

      mapBounds = new google.maps.LatLngBounds bounds.southWest, bounds.northEast

      maptiler = new klokantech.MapTilerMapType map, mapGetTile, mapBounds, 13, 18
      opacitycontrol = new klokantech.OpacityControl map, maptiler
      opacitycontrol.mb.setOpacity 0.6

      # because the opacitycontrol doesn't change the slider let's do this manually with a timeout =\
      setTimeout ->
        jQuery(".goog-slider-thumb").css "left", 36
      , 400

      map

    {
      getMap
      addWorkerMarker
      addAlertMarker
    }
