# see: http://mapicons.nicolasmollet.com/
@AlertType =
  medical:
    value: 1
    title: 'Eerste hulp'
    image: "/images/markers/firstaid.png"
  fire:
    value: 2
    title: 'Brandweer'
    image: "/images/markers/fire.png"
  police:
    value: 3
    title: 'Politie'
    image: "/images/markers/police.png"
