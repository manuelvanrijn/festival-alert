'use strict'

angular.module('festivalAlertServerApp', [])
  .config ['$routeProvider', ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/maps.html'
        controller: 'MapsCtrl'
      .otherwise
        redirectTo: '/'
  ]
