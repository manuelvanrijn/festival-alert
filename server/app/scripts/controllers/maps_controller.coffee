'use strict'

angular.module('festivalAlertServerApp')
  .controller 'MapsCtrl', ['$scope', 'MapsService', 'SocketService', ($scope, MapsService, socket) ->
    $scope.map = MapsService.getMap()
    $scope.alerts = []

    # Socket listeners
    # ================
    socket.subscribe 'alert', (data) ->
      marker = MapsService.addAlertMarker data.lat, data.lng, data.type
      alert =
        marker: marker
        type: data.type
        description: "some alert message"
      $scope.alerts.push alert

    $scope.showAlert = (alert) ->
      # animate the mark for a bit more focus after the panTo
      google.maps.event.addListenerOnce $scope.map, 'idle', () ->
        alert.marker.setAnimation google.maps.Animation.BOUNCE
        setTimeout ->
          alert.marker.setAnimation null
        , 730

      $scope.map.panTo alert.marker.getPosition()
      $scope.map.setZoom 17

    $scope.addWorker = ->
      MapsService.addWorkerMarker()

    $scope.addPolice = ->
      alert =
        marker: MapsService.addAlertMarker(null, null, AlertType.police)
        type: AlertType.police
        description: "some alert message?"

      $scope.alerts.push alert

    $scope.addMedical = ->
      alert =
        marker: MapsService.addAlertMarker(null, null, AlertType.medical)
        type: AlertType.medical
        description: "some alert message?"

      $scope.alerts.push alert

    $scope.addFire = ->
      alert =
        marker: MapsService.addAlertMarker(null, null, AlertType.fire)
        type: AlertType.fire
        description: "some alert message?"

      $scope.alerts.push alert
  ]
