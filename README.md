# Festival alert

## Dependencies

This project uses yeoman, grunt and bower. Please install these by running

```
npm install -g yo grunt-cli bower
```

## Server

To install/run the server you have to execute the following commands

```
cd server
npm install
bower install
grunt server
```

## Client

To install/run you have to install phonegap 3.0.0

```
sudo npm install -g phonegap
```

To run/build

```
cd client/www
bower install
cd ../../
phonegap local build ios
open client/platforms/ios/FestivalAlert.xcodeproj
```
